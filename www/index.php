<?php
$dev_settings = dirname(dirname(__FILE__)).'/.dev/override.php';
if (file_exists($dev_settings)) {
    require_once $dev_settings;
}
$saved_settings = dirname(__FILE__).'/saved_settings.php';
if (file_exists($saved_settings)) {
    require_once $saved_settings;
}
$CONF['css_framework'] = "bs3";
define('DEBUG_MODE', false);
define('YF_PATH', '/home/www/yf/');
define('WEB_PATH', '//gallery.dev/');
define('SITE_DEFAULT_PAGE', './?object=test');
define('SITE_ADVERT_NAME', 'YF Website');
require dirname(__FILE__).'/project_conf.php';
require YF_PATH.'classes/yf_main.class.php';
new yf_main('user', $no_db_connect = false, $auto_init_all = true);